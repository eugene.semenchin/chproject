// global // 

const section = document.querySelector('section');
const overlay = document.querySelector('.overlay');
const callbackOpen = document.querySelector('.callback__button');
const callbackModal = document.querySelector('.callback__modal');
const callbackClose = document.querySelector('.callback-close__button')

const callbackActive = function () {
    callbackModal.classList.toggle('active');
    overlay.classList.toggle('overlay--active');
};

const callbackHide = function () {
    callbackModal.classList.remove('active');
    overlay.classList.remove('overlay--active');
};

callbackOpen.addEventListener('click', function () {
    callbackActive()
});

callbackClose.addEventListener('click', function () {
    callbackHide()
});
// Свайпер Слайдеры // 

var swiper = new Swiper(".letters-thanks__slider", {
    slidesPerView: 2,
    spaceBetween: 30,
    speed: 500,
    loop: true,
    autoplay: {
        delay: 2200,
        disableOnInteraction: false,
    },

    pagination: {
      el: ".letters-thanks__pagination",
      clickable: true,
    },

    breakpoints: {
        375: {
            slidesPerView: 3,
        },

        576: {
            slidesPerView: 4,
        },

        768: {
            slidesPerView: 5,
        },

        1024: {
            slidesPerView: 6,
        },
    },
});

var swiper = new Swiper(".clients__slider", {
    slidesPerView: 3,
    spaceBetween: 40,
    speed: 500,
    loop: true,
    autoplay: {
        delay: 2200,
        disableOnInteraction: false,
    },

    breakpoints: {
        376: {
            slidesPerView: 3,
        },

        576: {
            slidesPerView: 4,
        },

        768: {
            slidesPerView: 5,
        },

        1024: {
            slidesPerView: 6,
        },
    },
});

var swiper = new Swiper(".stages__slider", {
    speed: 500,
    loop: true,
    autoplay: {
        delay: 3200,
        disableOnInteraction: false,
    },

    pagination: {
      el: ".stage__pagination",
      clickable: true,
      renderBullet: function (index, className) {
        return '<span class="' + className + '">' + (index + 1) + "</span>";
      },
    },
});

var swiper = new Swiper(".completed-work__slider", {
    slidesPerView: 2,
    spaceBetween: 30,
    speed: 500,
    loop: true,
    autoplay: {
        delay: 2200,
        disableOnInteraction: false,
    },

    pagination: {
      el: ".completed-work__pagination",
      clickable: true,
    },

    breakpoints: {
        375: {
            slidesPerView: 2,
        },

        576: {
            slidesPerView: 3,
        },

        768: {
            slidesPerView: 3,
        },

        1024: {
            slidesPerView: 4,
        },
    },
});

var swiper = new Swiper(".reviews__slider", {
    speed: 500,
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    navigation: {
        nextEl: ".reviews-button-next",
        prevEl: ".reviews-button-prev",
    },

    breakpoints: {
        320: {
            slidesPerView: 1,
            spaceBetween: 30,
        },

        675: {
            slidesPerView: 2,
            spaceBetween: 30,
        },

        768: {
            slidesPerView: 2,
            spaceBetween: 50,
        },

        1025: {
            slidesPerView: 1,
        },
    },
});

var swiper = new Swiper(".recommendations__slider", {
    slidesPerView: 2,
    spaceBetween: 15,
    speed: 500,
    loop: true,
    autoplay: {
        delay: 2200,
        disableOnInteraction: false,
    },

    pagination: {
      el: ".recommendations__pagination",
      clickable: true,
    },

    breakpoints: {
        680: {
            slidesPerView: 3,
            spaceBetween: 10,
        },

        1024: {
            slidesPerView: 4,
        },
    },
});
// Фильтры в каталоге // 

const filterMobileOpen = document.querySelector('.mobile-filter-open__button');
const filterModal = document.querySelector('.catalog__filter');
const filterMobileClose = document.querySelector('.mobile-filter-close__button');

const filterOpen = function() {
    filterModal.classList.toggle('catalog__filter--show');
}

const filterClose = function() {
       filterModal.classList.remove('catalog__filter--show');
}

if (section.classList.contains('page-catalog__section')) {
    filterMobileOpen.addEventListener('click', function() {
        filterOpen();
    })

    filterMobileClose.addEventListener('click', function() {
        filterClose();
    })
}
// Бургер Меню //

const burgerMobile = document.querySelector('.burger__mobile');
const headerNavigation = document.querySelector('.header__navigation');

const openMenu = function() {
    burgerMobile.classList.toggle('burger__mobile--active');
    headerNavigation.classList.toggle('header__navigation--active');
}

burgerMobile.addEventListener('click', function(e) {
    e.stopPropagation();
    openMenu();
});
// Табы в карточке товара //

const tabs = document.querySelector(".card-product__tabs");
const tabButton = document.querySelectorAll(".tab__button");
const contents = document.querySelectorAll(".tab__content");

if (section.classList.contains('card__section')) {
    tabs.onclick = e => {
        const id = e.target.dataset.id;
      
        if (id) {
          tabButton.forEach(btn => {
            btn.classList.remove("tab__active");
          });
          e.target.classList.add("tab__active");
      
          contents.forEach(content => {
            content.classList.remove("tab__active");
          });
          const element = document.getElementById(id);
          element.classList.add("tab__active");
        }
    }
}
// Кнопка наверх // 

const backToTop = document.querySelector('.back-to-top');
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 2500 || document.documentElement.scrollTop > 2500) {
        backToTop.classList.add('back-to-top--show');
    } else {
        backToTop.classList.remove('back-to-top--show');
    }
}

function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}