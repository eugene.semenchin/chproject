// Фильтры в каталоге // 

const filterMobileOpen = document.querySelector('.mobile-filter-open__button');
const filterModal = document.querySelector('.catalog__filter');
const filterMobileClose = document.querySelector('.mobile-filter-close__button');

const filterOpen = function() {
    filterModal.classList.toggle('catalog__filter--show');
}

const filterClose = function() {
       filterModal.classList.remove('catalog__filter--show');
}

if (section.classList.contains('page-catalog__section')) {
    filterMobileOpen.addEventListener('click', function() {
        filterOpen();
    })

    filterMobileClose.addEventListener('click', function() {
        filterClose();
    })
} 