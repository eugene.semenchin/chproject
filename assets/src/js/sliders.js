// Свайпер Слайдеры // 

var swiper = new Swiper(".letters-thanks__slider", {
    slidesPerView: 2,
    spaceBetween: 30,
    speed: 500,
    loop: true,
    autoplay: {
        delay: 2200,
        disableOnInteraction: false,
    },

    pagination: {
      el: ".letters-thanks__pagination",
      clickable: true,
    },

    breakpoints: {
        375: {
            slidesPerView: 3,
        },

        576: {
            slidesPerView: 4,
        },

        768: {
            slidesPerView: 5,
        },

        1024: {
            slidesPerView: 6,
        },
    },
});

var swiper = new Swiper(".clients__slider", {
    slidesPerView: 3,
    spaceBetween: 40,
    speed: 500,
    loop: true,
    autoplay: {
        delay: 2200,
        disableOnInteraction: false,
    },

    breakpoints: {
        376: {
            slidesPerView: 3,
        },

        576: {
            slidesPerView: 4,
        },

        768: {
            slidesPerView: 5,
        },

        1024: {
            slidesPerView: 6,
        },
    },
});

var swiper = new Swiper(".stages__slider", {
    speed: 500,
    loop: true,
    autoplay: {
        delay: 3200,
        disableOnInteraction: false,
    },

    pagination: {
      el: ".stage__pagination",
      clickable: true,
      renderBullet: function (index, className) {
        return '<span class="' + className + '">' + (index + 1) + "</span>";
      },
    },
});

var swiper = new Swiper(".completed-work__slider", {
    slidesPerView: 2,
    spaceBetween: 30,
    speed: 500,
    loop: true,
    autoplay: {
        delay: 2200,
        disableOnInteraction: false,
    },

    pagination: {
      el: ".completed-work__pagination",
      clickable: true,
    },

    breakpoints: {
        375: {
            slidesPerView: 2,
        },

        576: {
            slidesPerView: 3,
        },

        768: {
            slidesPerView: 3,
        },

        1024: {
            slidesPerView: 4,
        },
    },
});

var swiper = new Swiper(".reviews__slider", {
    speed: 500,
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    navigation: {
        nextEl: ".reviews-button-next",
        prevEl: ".reviews-button-prev",
    },

    breakpoints: {
        320: {
            slidesPerView: 1,
            spaceBetween: 30,
        },

        675: {
            slidesPerView: 2,
            spaceBetween: 30,
        },

        768: {
            slidesPerView: 2,
            spaceBetween: 50,
        },

        1025: {
            slidesPerView: 1,
        },
    },
});

var swiper = new Swiper(".recommendations__slider", {
    slidesPerView: 2,
    spaceBetween: 15,
    speed: 500,
    loop: true,
    autoplay: {
        delay: 2200,
        disableOnInteraction: false,
    },

    pagination: {
      el: ".recommendations__pagination",
      clickable: true,
    },

    breakpoints: {
        680: {
            slidesPerView: 3,
            spaceBetween: 10,
        },

        1024: {
            slidesPerView: 4,
        },
    },
});