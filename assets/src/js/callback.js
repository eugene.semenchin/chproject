const callbackOpen = document.querySelector('.callback__button');
const callbackModal = document.querySelector('.callback__modal');
const callbackClose = document.querySelector('.callback-close__button')

const callbackActive = function () {
    callbackModal.classList.toggle('active');
    overlay.classList.toggle('overlay--active');
};

const callbackHide = function () {
    callbackModal.classList.remove('active');
    overlay.classList.remove('overlay--active');
};

callbackOpen.addEventListener('click', function () {
    callbackActive()
});

callbackClose.addEventListener('click', function () {
    callbackHide()
});
