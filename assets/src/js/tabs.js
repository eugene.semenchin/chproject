// Табы в карточке товара //

const tabs = document.querySelector(".card-product__tabs");
const tabButton = document.querySelectorAll(".tab__button");
const contents = document.querySelectorAll(".tab__content");

if (section.classList.contains('card__section')) {
    tabs.onclick = e => {
        const id = e.target.dataset.id;
      
        if (id) {
          tabButton.forEach(btn => {
            btn.classList.remove("tab__active");
          });
          e.target.classList.add("tab__active");
      
          contents.forEach(content => {
            content.classList.remove("tab__active");
          });
          const element = document.getElementById(id);
          element.classList.add("tab__active");
        }
    }
}

