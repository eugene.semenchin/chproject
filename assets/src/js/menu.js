// Бургер Меню //

const burgerMobile = document.querySelector('.burger__mobile');
const headerNavigation = document.querySelector('.header__navigation');

const openMenu = function() {
    burgerMobile.classList.toggle('burger__mobile--active');
    headerNavigation.classList.toggle('header__navigation--active');
}

burgerMobile.addEventListener('click', function(e) {
    e.stopPropagation();
    openMenu();
});

